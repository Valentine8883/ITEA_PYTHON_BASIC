"""Написать программу для подсчета количества дней между
двумя введенными с клавиатуры датами (см. модуль datetime)
"""
from datetime import date


def get_date():
    date_dict = {'year': 0, 'month': 0, 'day': 0}
    for key in date_dict.keys():
        while True:
            try:
                date_dict[key] = int(input("{0} - ".format(key)))
                if date_dict['month'] > 12:
                    print('month must not be more than 12, try again')
                    continue
                elif date_dict['day'] > 31:
                    print('day must not be more than 31, try again')
                    continue
                elif date_dict['year'] > 9999:
                    print('year must not be more than 9999, try again')
                    continue
                break
            except ValueError:
                print('Invalid data for the {0}, try again'.format(key))
    return date(date_dict['year'], date_dict['month'], date_dict['day'])


def get_date_difference(first, second):
    if first > second:
        first, second = second, first
    return (second - first).days


print('Please, enter the first date')
first_date = get_date()

print('Please, enter the second date')
second_date = get_date()

print('The difference between the first and second date -',
      get_date_difference(first_date, second_date), 'days.')
