"""Написать программу, которая играет с пользователем в игру “Угадай число”.
В данном случае число будет загадывать пользователь, а программа - угадывать.
"""
from random import randint


def get_number(some_text):
    while True:
        number = input(some_text)
        try:
            number = int(number)
            break
        except ValueError:
            print("\nInvalid data try again")
    return number


def get_answer():
    while True:
        answer = str(input("So, your number is greater than,"
                           " less than, or I'm guessing [>\<\=]:\n"))
        if answer in '<>=':
            break
        else:
            print('Wrong answer, try again')
    return answer


start_range = get_number('Enter the start of a range\n')
end_range = get_number('Enter the end of a range\n')
if end_range < start_range:
    end_range, start_range = start_range, end_range
print('You guess the number from', start_range, 'to', end_range)

while True:
    if start_range == end_range:
            print("The guess number is", start_range)
            break
    guess_number = randint(start_range, end_range)
    print('The guess number is', guess_number)
    question = get_answer()
    if question == ">":
        start_range = guess_number + 1
    elif question == "<":
        end_range = guess_number - 1
    elif question == "=":
        print("I'm guessing!!!")
        break
