""" Написать интерактивный калькулятор, который позволит
вводить 2 числа и операцию и получать результат вычислений.
"""


def get_float_number(some_text):
    while True:
        num = input(some_text)
        try:
            num = float(num)
            break
        except ValueError:
            print('Incorrect data, try again')
    return num


def get_operation():
    while True:
        answer = str(input('What operations need to be performed'
                           ' with these two numbers:[*, /, +, -, **,]\n'))
        if answer in '**/+-':
            break
        else:
            print('Incorrect answer, try again')
    return answer


def get_result(f_num, s_num, operation):
    try:
        if operation == "+":
            result = f_num + s_num
        elif operation == '-':
            result = f_num - s_num
        elif operation == '/':
            result = f_num / s_num
        elif operation == '*':
            result = f_num * s_num
        elif operation == '**':
            result = f_num ** s_num
    except ZeroDivisionError:
        print("You can not divide by zero")
    else:
        print('{0} {1} {2} = {3}'.format(f_num, operation,
                                         s_num, result))


first_num = get_float_number('Enter the first number\n')
second_num = get_float_number('Enter the second number\n')
operation = get_operation()
get_result(first_num, second_num, operation)

