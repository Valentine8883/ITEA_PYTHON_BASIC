"""Написать рекурсивную функцию, которая возвращает список,
переданный в качестве параметра, в обратном порядке"""


def revers_func():
    output_list = []

    def intern_revers(input_list):
        if input_list:
            output_list.insert(0, input_list[0])
            intern_revers(input_list[1:])
        return output_list
    return intern_revers


my_list = [0, 1, 2, 3, 4, 5]
print('List in revers - ', revers_func()(my_list))

