"""Написать собственную реализацию функции reduce()"""
from functools import reduce


def my_reduce(some_func, some_list):
    res = some_func(some_list[0], some_list[1])
    for i in range(len(some_list)-2):
        res = some_func(res, some_list[i+2])
    return res


my_list = [5, 4, 3, 2, 1]

print('Standard reduce function - ', reduce(lambda x, y: x * y, my_list))
print('My reduce function - ', my_reduce(lambda x, y: x * y, my_list))
