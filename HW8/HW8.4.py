"""Написать декоратор, который для функции с произвольным
количеством параметров выдает на экран список значений
переданных параметров и результат выполнения функции"""


def decorator(func):
    def wrapper(*args):
        print(' Input number -  ', *args, '\n', end=' ')
        print('Root of a num - ', end=' ')
        func(*args)
    return wrapper


@decorator
def root(*args):
    for i in args:
        print(i * i, end=' ')


root(2, 4, 7, 9)

