"""Написать собственную реализацию функции filter()"""


def my_filter(some_func, some_list):
    output_list = []
    for item in some_list:
        if some_func(item):
            output_list.append(item)
    return output_list


my_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

print('Standard filter function - ', list(filter(lambda x: x > 5, my_list)))
print('My filter function  - ', list(my_filter(lambda x: x > 5, my_list)))


