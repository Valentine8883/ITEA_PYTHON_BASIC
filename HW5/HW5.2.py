# Написать программу для подсчета количества различных букв в тексте


def get_string(some_string):
    return input(str(some_string))


def count_character(some_string):
    converted_string = ""
    for i in some_string:
        if chr(96) < i < chr(123) or chr(64) < i < chr(91):
            converted_string += i.lower()
    return {character: converted_string.count(character)
            for character in converted_string}


def print_dictionary(some_dictionary):
    for key, value in some_dictionary.items():
        print("'{0}' - {1}".format(key, value))


my_string = get_string(
    "Please, enter some text\n")
print_dictionary(count_character(my_string))


