# Для введенной с клавиатуры строки проверить, является ли она действительным числом


def get_string(some_string):
    return input(str(some_string))


def is_it_real_number(some_string):
    from re import match
    if match(r'\d'+ ',' + '\d', some_string):
        print("The string is a number")
    elif match(r'\d', some_string):
        print("The string is a number")
    else:
        print("The string is not a number")


my_string = get_string(
    "Please, enter the string\n")

is_it_real_number(my_string)
