# Написать программу подсчета гласный и согласных букв в тексте


def get_string(some_string):
    return input(str(some_string))


def count_vowel_consonant(some_string):
    vowel_counter = 0
    consonant_counter = 0
    vowel_chrs = "aeiou"
    for i in some_string:
        if chr(96) < i < chr(123) or chr(64) < i < chr(91):
            if i in vowel_chrs or i in vowel_chrs.upper():
                vowel_counter += 1
            else:
                consonant_counter += 1
    return vowel_counter, consonant_counter


my_string = get_string(
    'Please, enter some text\n')

count_vowel, count_consonant = count_vowel_consonant(my_string)

print("The total number of vowels - {0}\n"
      "The total number of consonants - {1}".format(count_vowel, count_consonant))
