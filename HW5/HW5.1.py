# Написать программу для изменения первой буквы каждого второго слова на заглавную


def get_string(some_string):
    return input(str(some_string))


def convert_sentence(some_string):
    converted_string = ""
    for idx, word in enumerate(
            some_string.split(" ")):
        if idx % 2 != 0:
            word = word.capitalize()
        converted_string += word
        if idx < (len(some_string.split(" "))) - 1:
            converted_string += " "
    return str(converted_string)


my_string = get_string(
    "Please, enter some sentence\n")

print("The initial string   -", my_string,
      "\nThe converted string -", convert_sentence(my_string))
