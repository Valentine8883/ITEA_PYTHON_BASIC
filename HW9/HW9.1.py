class Shape(object):
    def __init__(self, pos):
        self.pos = pos

    def __str__(self):
        return str(self.pos)


class Circle(Shape):
    def __init__(self, pos, radius):
        self.radius = radius
        super().__init__(pos)

    def get_circle_area(self, radius):
        return radius * 3.14


class Square(Shape):
    def __init__(self, pos, side):
        self.side = side
        super().__init__(pos)

    def get_square_area(self, side):
        return side * side


class Triangle(Shape):
    def __init__(self, pos, side_a, side_b, side_c):
        self.side_a = side_a
        self.side_b = side_b
        self.side_c = side_c
        super().__init__(pos)

    def get_triangle_area(self, side_a, side_b, side_c):
        p = (side_a + side_b + side_c) / 2
        return (p * (p - side_a) * (p - side_b) * (p - side_c)) ** 0.5


print(Square.get_square_area(1, 2))
print(Triangle.get_triangle_area(2, 3, 4, 5))
print(Circle.get_circle_area(3, 4))
