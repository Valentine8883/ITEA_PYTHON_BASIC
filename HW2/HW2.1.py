
def get_area(a, b, c):
    p = (a+b+c) / 2
    return (p * (p-a) * (p-b) * (p-c)) ** 0.5


def get_side_length(number):
    return float(input(number))


print("Please, enter sides of the triangle")

a = get_side_length("The side A = ")
b = get_side_length("The side B = ")
c = get_side_length("The side C = ")

if (a + b) > c or (a + c) > b or (c + b) > a:
    print("The area of the triangle = ", get_area(a, b, c))
else:
    print('Sorry, but the triangle does not exist')
