def get_factorial(x):
    if x == 0:
        return 1
    else:
        return x * get_factorial(x - 1)


def get_number(number):
    return int(input(number))


n = get_number("Enter the number\n")

print(n, "! = ", get_factorial(n))

