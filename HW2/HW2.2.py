def get_max(a, b, c):
    return a if b and c < a else b if a and c < b else c


def get_number(number):
    return float(input(number))


x = get_number("Enter the first number\n")
y = get_number("Enter the second number\n")
z = get_number("Enter the third number\n")

print("The maximum number is ", get_max(x, y, z))




