"""Написать программу, которая выводит числа от 1 до 100, но
значения, которые делятся на 3 заменяет на “Fizz”, которые делятся
на 5 - на “Buzz”, а которые делятся на 15 - на “FizzBuzz”"""


def fizzbuzzd_number(n):
    if n % 3 == 0:
        res = "Fizz"
        if n % 5 == 0:
            res += "Buzz"
    elif n % 5 == 0:
        res = "Buzz"
    else:
        res = n
    return res


def output_fizzbuzz(i, n):
    while i <= n:
        print('{0} = {1}'.format(i, fizzbuzzd_number(i)))
        i += 1


output_fizzbuzz(1, 100)