"""Написать программу для вывода на экран таблицы умножения до 10"""


def output_multiplication_table(n, m):
    i = 1
    while i <= n:
        j = 1
        while j <= m:
            print('{0} × {1} = {2}'.format(i, j, i * j))
            j += 1
        i += 1
        print("__________\n")


output_multiplication_table(9, 9)

