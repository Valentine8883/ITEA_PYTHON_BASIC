"""Написать программу для расчета факториала n! = 1 * 2 * 3 * ... * n"""


def get_fact(n):
    i = 1
    res = 1
    while i <= n:
        res = i * res
        i += 1
    return int(res)


def get_number(x):
    return int(input(x))


num = get_number("Please, enter the number\n")

print('{0}! = {1}'.format(num, get_fact(num)))
