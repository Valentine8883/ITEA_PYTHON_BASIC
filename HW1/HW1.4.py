
def define_values(n):
    if n % 2 == 0:
        return n*n
    else:
        return n*2


def get_number(number):
    return int(input(number))


x = get_number("Please, enter the number \n")

print(define_values(x))