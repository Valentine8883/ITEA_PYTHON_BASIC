def get_area(a, b, c):
    p = (a+b+c) / 2
    return (p * (p-a) * (p-b) * (p-c)) ** 0.5


def get_number(number):
    return float(input(number))


x = get_number("The side a = ")
y = get_number("The side b = ")
z = get_number("The side c = ")

print('The area of the triangle = ', get_area(x, y, z))

