"""Написать программу, которая для двух файлов, которые содержат числа, упорядоченные
по возрастанию, создает третий файл, содержащий упорядоченные числа из обоих файлов.
"""
from random import randint


def create_rand_file(file_name, length):
    some_list = []
    for idx in range(length):
        some_list.append(randint(1, 99))
    some_list.sort()
    with open(file_name, 'w') as file:
        for idx in range(len(some_list)):
            file.write(str(some_list[idx]))
            if idx < len(some_list) - 1:
                file.write(' ')


def show_me_the_file(file_mame):
    with open(file_mame, 'r') as file:
        print(file_mame, ' - ', (file.read()))


def merging_two_files(first_filename, second_filename,
                      output_filename):
    with open(first_filename, 'r') as file:
        f_list = (file.read()).split()
    with open(second_filename, 'r') as file:
        s_list = (file.read()).split()
    t_list = f_list + s_list
    for idx, item in enumerate(t_list):
        try:
            t_list[idx] = int(item)
        except ValueError:
            print('Invalid file format')
            break
    t_list.sort()
    with open(output_filename, 'w') as file:
        for item in t_list:
            file.write(str(item) + ' ')


create_rand_file('HW7.2.1.txt', 5)
show_me_the_file('HW7.2.1.txt')
create_rand_file('HW7.2.2.txt', 5)
show_me_the_file('HW7.2.2.txt')

merging_two_files('HW7.2.1.txt', 'HW7.2.2.txt',
                  'HW7.2.3.txt')

show_me_the_file('HW7.2.3.txt')
