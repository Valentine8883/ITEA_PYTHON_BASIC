"""Написать программу для копирования содержимого
файла в новый файл в обратном порядке
"""


def create_file(filename):
    with open(filename, 'w') as file:
        for i in range(6):
            for j in range(6):
                file.write('{0}{1} '.format(i, j))
            file.write('\n')


def show_me_the_file(filename):
    with open(filename, 'r') as file:
        print(file.read())


def revers_copy(input_filename, output_filename):
    output_file = open(output_filename, 'w')
    input_file = open(input_filename, 'r')
    output_file.write(input_file.read())
    print(input_file.tell())
    input_file.close()
    output_file.close()


create_file('HW7.3.1.txt')
show_me_the_file('HW7.3.1.txt')


output_file = open('HW7.3.2.txt', 'wb')
input_file = open('HW7.3.1.txt', 'rb')


input_file.seek(-1, 2)
r = 0
i = 0
while i < 3:
    input_file.seek(-r, 1)
    output_file.write(input_file.read())
    i += 1
    r += 2

output_file.close()

show_me_the_file('HW7.3.2.txt')



