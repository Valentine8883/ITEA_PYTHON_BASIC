"""Есть файл следующего формата: “название товара” “цена за штуку” “количество”
(поля разделены пробелами). Написать программу для подсчета стоимости всех товаров в файле.
"""


def create_file(some_dict, file_name):
    with open(file_name, 'w') as file:
        for key, item in some_dict.items():
            try:
                file.write('{0} {1[0]} {1[1]} \n'.format(key, item))
            except IndexError:
                print('Incorrect data for {}'.format(key))
                break


def show_total_sum(file_name):
    total_sum = 0
    with open(file_name, 'r') as file:
        for idx, line in enumerate(file):
            prod_list = line.split()
            try:
                float(prod_list[1]) and int(prod_list[2])
            except ValueError:
                print('Data from line number {} cannot be read'.format(idx+1))
                total_sum = None
                break
            else:
                print('\t{1}. {0[0]} - {0[1]}$, {0[2]}'.format(prod_list, idx+1))
                total_sum += float(prod_list[1]) * int(prod_list[2])
        print('\nTotal sum - {}$'.format(total_sum))


order_dict = {'Burger': [12.5, 1], 'Cola': [16.55, 1], 'Chicken': [5.5, 1],
              'Coffee': [7.5, 2], 'Pizza': [6.5, 2], 'Water': [2.0, 2],
              'Cake': [3.0, 2], 'Salad': [6.5, 2], 'Juice': [5.5, 2]}

create_file(order_dict, 'HW7.1.txt')
show_total_sum('HW7.1.txt')
