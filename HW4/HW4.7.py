# Найти сумму элементов списка между двумя первыми 0. Если 0 нет, то вывести 0


def random_list(a, b, r):
    import random
    return [random.randrange(a,b) for item in range(r)]


def sum_between_zeros(some_list):
    if some_list.count(0) > 1:
        sum_ = 0
        for idx in range(some_list.index(0) + 1, len(some_list), 1):
            if some_list[idx] != 0:
                sum_ += some_list[idx]
            else:
                break
    else:
        sum_ = 0
    return sum_


my_list = random_list(0, 5, 30)
print("The initial list -", my_list, "\nSum =", sum_between_zeros(my_list))


