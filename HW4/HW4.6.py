#Удалить из списка числа, которые повторяются более 2-х раз


def random_list(a, b, r):
    import random
    return [random.randrange(a, b) for item in range(r)]


def remove_duplicate(some_list, n):
    i = 0
    while i < len(some_list):
        if some_list.count(some_list[i]) > n:
            some_list.remove(some_list[i])
            i -= 1
        i += 1
    return some_list


my_list = random_list(1, 5, 25)
print("The initial list -", my_list)
print("\nThe converted list -", remove_duplicate(my_list, 2))


