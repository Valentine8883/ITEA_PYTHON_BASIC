#Найти произведение элементов списка


def random_list(a, b, r):
    from random import randrange as my_randrange
    return [my_randrange(a, b) for item in range(r)]


def product_items(some_list):
    result = 1
    for item in some_list:
        result *= item
    return result


my_list = random_list(1, 5, 4)

print("The list -", my_list, "\n\nThe product of all element of the list = ", product_items(my_list))