# Написать программу для поиска значения в отсортированном списке методом деления пополам


def random_list(a, b, r):
    import random
    return [random.randrange(a, b) for item in range(r)]


my_list = [1, 2, 3, 4, 5, 6, 8]

my_list.sort()
print(my_list)
n = 5

begin = 0
end = len(my_list)
l = begin + end // 2
num = my_list[begin + end // 2]
while begin != end:
    if n == my_list[begin + end // 2]:
        print("The number is found, index =", my_list.index(n))
        break
    elif n > my_list[begin + end // 2]:
        begin = begin + end // 2
    elif n > begin + end // 2:
        end = begin + end // 2