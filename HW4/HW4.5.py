#Написать программу для обхода матрицы по главной диагонали

my_list = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
    ]

for i in range(len(my_list)):
    for j in range(len(my_list[i])):
        if i == j:
            print(my_list[i][j], end="  ")
        else:
            print("-", end="  ")
    print()

