#Переставить минимальный и максимальный элементы списка местами


def random_list(a, b, r):
    import random
    return [random.randrange(a, b) for item in range(r)]


def search_min_idx_max_idx(some_list):
    max_ = some_list[0]
    min_ = some_list[0]
    idx_max = 0
    idx_min = 0
    for idx, item in enumerate(some_list):
        if item >= max_:
            max_ = item
            idx_max = idx
        if item <= min_:
            min_ = item
            idx_min = idx
    return min_, idx_min, max_, idx_max


my_list = random_list(1, 100, 6)
min_value, index_min, max_value, index_max = search_min_idx_max_idx(my_list)

print("The initial list -", my_list, "\n\nMin = {0}, Max = {1}".format(min_value, max_value))

my_list[index_min] = max_value
my_list[index_max] = min_value

print("\nThe converted list -", my_list)
