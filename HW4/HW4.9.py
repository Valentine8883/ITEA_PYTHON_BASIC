# Выполнить циклический сдвиг влево на 3 элемента части списка до первого 0 и вправо - после первого 0


def shift_left_right(some_list, l, r):
    for i in range(l):
        for idx in range(some_list.index(0) - 1, 0, -1):
            some_list[idx], some_list[0] = some_list[0], some_list[idx]
    for j in range(r):
        for idx in range(some_list.index(0) + 1, len(some_list), 1):
            some_list[len(some_list) - 1], some_list[idx] = some_list[idx], some_list[len(some_list) - 1]
    return some_list


my_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

print("The initial list   -", my_list)
print("The converted list -", shift_left_right(my_list, 3, 3))
