#Найти минимальный и максимальный элемент списка


def random_list(a, b, r):
    import random
    return [random.randrange(a, b) for item in range(r)]


def search_min_max(some_list):
    max_ = some_list[0]
    min_ = some_list[0]
    for item in some_list:
        if item >= max_:
            max_ = item
        if item <= min_:
            min_ = item
    return min_, max_


my_list = random_list(-10, 10, 5)
min_value, max_value = search_min_max(my_list)

if min_value == max_value:
    print("The list -", my_list, "\n\nElements of the list are equal")
else:
    print("The list -", my_list, "\n\nMin = {0}, Max = {1}".format(min_value, max_value))

