#Написать программу для обхода матрицы (2-мерного списка) по строкам и столбцам

my_list = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
    ]

for i in range(len(my_list)):
    for j in range(len(my_list[i])):
        print(my_list[i][j], end="   ")
    print("\n")

print()

for i in range(len(my_list)):
    for j in range(len(my_list[i])):
        print(my_list[j][i], end="   ")
    print("\n")


